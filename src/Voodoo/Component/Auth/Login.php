<?php
/** Voodoo\Component
 ******************************************************************************
 * @desc        Login adds login functionalities to your application. Just extends it to your model
 * @package     Voodoo\Component\Auth
 * @name        Login
 * @copyright   (c) 2013
 ******************************************************************************/

namespace Voodoo\Component\Auth;

use Voodoo\Core,
    Voodoo\Component\Encryptor\Encryptor;

abstract class Login extends Session
{
    CONST OAUTH_FACEBOOK = "facebook";
    CONST OAUTH_TWITTER = "twitter";
    CONST OAUTH_GOOGLE = "google";
    
    /**
     * To login
     *
     * @param type $login
     * @param type $password
     * @return type
     * @throws Exception
     */
    public function loginWithEmail($email, $password)
    {
        if(! Core\Helpers::validEmail($email)) {
            throw new Exception("Email: {$email} is invalid");
        }

        $user = $this->reset()->where(["email" => $email, "display" => 1])->findOne();
        if ($user) {
            $password = trim($password);
            $dPassword = (new Encryptor)->decrypt($user->password);
            if( $password != $dPassword) {
                throw new Exception("Invalid password for: {$email}");
            }
            $user->updateLastLogin();
            
            return $user;
        } else {
            throw new Exception("Invalid Login: {$email}");
        }
    }
    
    /**
     * Update the last login datetime
     * 
     * @return \Voodoo\Component\Auth\Login
     */
    public function updateLastLogin()
    {
        if ($this->isSingleRow()) {
            $this->update(["datetime_last_login" => $this->NOW()]);
        }
        return $this;
    }
    
    /**
     * Register new Account
     *
     * @param type $email
     * @param type $password
     * @throws Exception
     */
    public function registerWithEmail($email, $password, $screen_name = "")
    {
        if(! Core\Helpers::validEmail($email)) {
            throw new Exception("Email: {$email} is invalid");
        }
        if(! Core\Helpers::validPassword($password)) {
            throw new Exception("Invalid password");
        }

        $email = $this->formatEmail($email);
        $screen_name = trim(strip_tags($screen_name));
        if(! $this->emailExists($email)) {
            $password = trim($password);
            $ePassword = (new Encryptor)->encrypt($password);
            return $this->insert([
                        "screen_name" => $screen_name,
                        "email" => $email,
                        "password" => $ePassword,
                        "display" => 1,
                        "datetime_created" => $this->NOW(),
                        "datetime_updated" => $this->NOW()
                    ]);
        } else {
            throw new Exception("Email: {$email} exists already");
        }
    }


    public function loginWithOAuth($oauthType)
    {
        
    }
    
    public function signupWithOAuth($oauthType, $uid, $token = "", $token_secret = "", $screen_name = "")
    {
        $this->set([
            "{$oauthType}_uid" => $uid,
            "{$oauthType}_token" => $token,   
            "{$oauthType}_token_secret" => $token_secret,
            "{$oauthType}_screen_name" => $screen_name,
        ])->save();
    }
    
    
    /**
     * To reset a password and return the newly generated one
     * @param string $email
     * @return string
     * @throws Exception
     */
    public function resetPassword($email) {
        if(! Core\Helpers::validEmail($email)) {
            throw new Exception("Email: {$email} is invalid");
        }
        $user = $this->reset()->where(["email" => $email])->findOne();
        if ($user) {
            $password = strtolower(trim(Core\Helpers::generateRandomString()));
            $ePassword = (new Encryptor)->encrypt($password);

            $user->update([
                "password" => $ePassword,
                "datetime_updated" => $this->NOW()
            ]);
            
            return $password;
        } else {
            throw new Exception("Invalid Login: {$email}");
        }
    }


    /**
     * Change the password
     *
     * @param type $password
     * @return \App\Www\Adminzone\Model\Admin\User
     * @throws Exception
     */
    public function changePassword($password)
    {
        if(! Core\Helpers::validPassword($password)) {
            throw new Exception("Invalid password");
        } else {
            $password = trim($password);
            $ePassword = (new Encryptor)->encrypt($password);
            $this->update(["password" => $ePassword]);
        }
        return $this;
    }

    /**
     * Change the login email
     * 
     * @param type $email
     * @return \App\Www\Adminzone\Model\Admin\User
     * @throws Exception
     */
    public function changeEmail($email)
    {
        if(! Core\Helpers::validEmail($email)) {
            throw new Exception("Email: {$email} is invalid");
        } else {
            $email = $this->formatEmail($email);
            if(! $this->emailExists($email)) {
                $this->update(["email" => $email]);
            } else {
                throw new Exception("Email: {$email} exists already");
            }
        }
        return $this;
    }

    /**
     * Disable an account
     * @return \App\Www\Adminzone\Model\Admin\User
     */
    public function deactivate()
    {
        if($this->isSingleRow()) {
            $this->update(["display" => 0]);
        }
        return $this;
    }

    /**
     * Enable an account
     * @return \App\Www\Adminzone\Model\Admin\User
     */
    public function activate()
    {
        if($this->isSingleRow()) {
            $this->update(["display" => 1]);
        }
        return $this;
    }


    /**
     * Check if email exists
     *
     * @param type $email
     * @return type
     * @throws Exception
     */
    public function emailExists($email)
    {
        $email = $this->formatEmail($email);
        if(Core\Helpers::validEmail($email)) {
            $user = (new static)->where(["email" => $email])->findOne();
            return ($user) ? true : false;
        } else {
            throw new Exception("Invalid email: {$email}");
        }
    }

    /**
     * Check if a password match the current password
     * @param string $password
     * @return bool
     */
    public function passwordMatch($password)
    {
        $password = trim($password);
        $dPassword = (new Encryptor)->decrypt($this->password);
        return $password === $dPassword;      
    }
    
    /**
     * Prepare the email to be processed
     *
     * @param type $email
     * @return string
     */
    private function formatEmail($email)
    {
        return trim(strtolower($email));
    }
    
/*******************************************************************************/
    final public function installLoginTable()
    {
        $sql = "
                CREATE TABLE `{$this->tableName}` (
                    `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
                    `screen_name` VARCHAR(50) NOT NULL DEFAULT '0',
                    `email` VARCHAR(50) NOT NULL DEFAULT '0',
                    `password` VARCHAR(75) NOT NULL DEFAULT '0',
                    `facebook_uid` VARCHAR(50) NULL DEFAULT NULL,
                    `facebook_token` VARCHAR(125) NULL DEFAULT NULL,
                    `facebook_token_secret` VARCHAR(250) NULL DEFAULT NULL,
                    `twitter_uid` VARCHAR(50) NULL DEFAULT NULL,
                    `twitter_token` VARCHAR(50) NULL DEFAULT NULL,
                    `twitter_token_secret` VARCHAR(250) NULL DEFAULT NULL,
                    `twitter_screen_name` VARCHAR(50) NULL DEFAULT NULL,
                    `google_uid` VARCHAR(50) NULL DEFAULT NULL,
                    `google_token` VARCHAR(50) NULL DEFAULT NULL,
                    `google_token_secret` VARCHAR(250) NULL DEFAULT NULL,
                    `display` TINYINT(1) NOT NULL DEFAULT '0',
                    `datetime_last_login` DATETIME NOT NULL,
                    `datetime_created` DATETIME NOT NULL,
                    `datetime_updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                    PRIMARY KEY (`id`),
                    INDEX `email` (`email`),
                    INDEX `display` (`display`),
                    INDEX `facebook_uid` (`facebook_uid`),
                    INDEX `twitter_uid` (`twitter_uid`),
                    INDEX `google_uid` (`google_uid`)
                )    
        ";
        $this->query($sql, [], true);
    }
   
}
