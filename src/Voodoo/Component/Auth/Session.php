<?php
/** Voodoo\Component
 ******************************************************************************
 * @desc        Session adds session abilities to the logins. It is managed by MySQL
 * @package     Voodoo\Component\Auth
 * @name        Session
 * @copyright   (c) 2013
 ******************************************************************************/

namespace Voodoo\Component\Auth;

use Voodoo;

abstract class Session extends Voodoo\Component\Model\BaseModel
{
    private $loginModel = null;
    
    protected function setup()
    {
        parent::setup();
        $this->timestampable(false);
    }
    
    /**
     * Return the login connected to the session
     * 
     * @return Lib\Model\Model
     */
    public function getSession()
    {
        if(! $this->loginModel) {
            $session = $this->getCurrentSession();
            if($session) {
                $userId = $session->{$this->tableName()."_id"};
                $this->loginModel = (new static)->findOne($userId);
            } else {
                return false;
            }            
        }
        return $this->loginModel;
    }
    
    
    /**
     * Create a session
     * 
     * @return Lib\Model\Model
     */
    public function createSession()
    {
        if($this->isSingleRow()) {
            $sessionId = Voodoo\Core\Helpers::getNonce();
            $this->getSessionTable()->reset()
                                    ->where(["{$this->tableName()}_id" => $this->getPK()])
                                    ->delete();
            
            $expireTime = time() + (Voodoo\Core\Config::Component()->get("Auth.sessionTTL"));
            $this->getSessionTable()->insert([
                "{$this->tableName()}_id" => $this->getPK(),
                "session_id" => $sessionId,
                "ip" => Voodoo\Core\Http\Request::getIp(),
                "datetime_expire" => date("Y-m-d H:i:s", $expireTime)
            ]);
            setcookie($this->getSessionCookieName(), $sessionId, $expireTime, "/");
            return $this;
        } else {
            return false;
        }
    }

    /**
     * Destroy a session
     * 
     * @return bool
     */
    public function destroySession()
    {
        if($this->isSingleRow()) {
            $this->loginModel = null;
            $this->getCurrentSession()->delete();      
            setcookie($this->getSessionCookieName(), "", 0, "/");
            return true;
        }
        return false;
    }


    /**
     * return the current session entry
     * 
     * @return self
     */
    public function getCurrentSession()
    {
        $sessionId = $_COOKIE[$this->getSessionCookieName()];
        $sessionTable = $this->getSessionTable();
        return $sessionTable->where([
                                    "session_id" => $sessionId,
                                    "datetime_expire > ? " => $this->NOW()
                             ])->findOne();        
    }
    
    /**
     * Get the session's table
     * 
     * @return Lib\Model\Model
     */
    private function getSessionTable()
    {
        return $this->table("{$this->getTablename()}_session");
    }
    
    /**
     * Return the cookie session name
     * 
     * @return string
     */
    private function getSessionCookieName()
    {
        $name = Voodoo\Core\Config::Component()->get("Auth.sessionName") 
                ?: (md5($this->getTablename()."_session"));
        return $name;
    }

    /**
     * Drop all sessions
     * 
     * @return boolean
     */
    public function dropAllSessions()
    {
        $this->getSessionTable()->delete();
        return true;
    }
    
    
    /**
     * Drop all expired
     * 
     * @return boolean
     */
    public function dropAllExpired()
    {
        $this->getSessionTable()
                ->whereLt("datetime_expire", $this->NOW())
                ->delete();
        return true;
    }
    
    /**
     * Install the session table
     */
    final public function installSessionTable()
    {
        $sql = "
            CREATE TABLE `{$this->getTablename()}_session` (
                `id` INT(10) NOT NULL AUTO_INCREMENT,
                `{$this->tableName()}_id` INT(10) NOT NULL,
                `session_id` CHAR(45) NOT NULL,
                `ip` VARCHAR(250) NOT NULL,
                `datetime_expire` DATETIME NOT NULL,              
                PRIMARY KEY (`id`),
                INDEX `session_id` (`session_id`),
                INDEX `user_id` (`user_id`)
            )
        "; 
        $this->query($sql, [], true);
    }    
}
